/*
Створити об'єкт "Документ", де визначити властивості "Заголовок, тіло, футер, дата".
Створити вкладений об'єкт - "Додаток".
Створити об'єкт "Додаток",  зі вкладеними об'єктами, "Заголовок, тіло, футер, дата".
Створити методи для заповнення та відображення документа. використовуючі оператор in
*/
const newDocument = {
  header: "This is a header for the homework",
  body: "This is a body for the homework 5",
  footer: "This is a footer for the homework 5",
  date: "28Dec2022",
  appendix: {
      header: "This is a header for the document",
      body: "This is a body for the document",
      footer: "This is a footer for the document",
      date: "28Dec2022",
  },
}
function completeMainData(data) {
switch (data) {
        case "header":
          data = prompt("please enter data for Header for the main section");
          document.write(`<header style = "color:red">${data}</header>`, "<br />");
          break
        case "body":
          data = prompt("please enter data for body for the main section");
          document.write(`<body style = "color:black">${data}</body>`, "<br />");
          break
        case "footer":
          data = prompt("please enter data for footer for the main section");
          document.write(`<footer style = "color:blue">${data}</footer>`, "<br />");
          break
        default:
          data = prompt("please enter date for the main section");
          document.write(`<p style = "color:black">${data}</p>`, "<br />");
          break
      }
}
function completeSubData(data) {
switch (data) {
        case "header":
          data = prompt("please enter data for Header for the sub-section");
          document.write(`<header style = "color:red">${data}</header>`, "<br />");
          break
        case "body":
          data = prompt("please enter data for body for the sub-section");
          document.write(`<body style = "color:black">${data}</body>`, "<br />");
          break
        case "footer":
          data = prompt("please enter data for footer for the sub-section");
          document.write(`<footer style = "color:blue">${data}</footer>`, "<br />");
          break
        default:
          data = prompt("please enter date for the sub-section");
          document.write(`<p style = "color:black">${data}</p>`, "<br />");
          break
      }
}

function displayData(obj) {
  for (let data in obj) {
    if (typeof obj[data] !== 'object') {
      let elName = data;
      completeMainData(elName);
    } else {
      for (let data2 in obj[data]) {
        let elSecondName = data2;
        completeSubData(elSecondName);
      }
    }
  }
}

displayData(newDocument);