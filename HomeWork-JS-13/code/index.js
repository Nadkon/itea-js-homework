/*
Створіть сайт з коментарями. Коментарі тут : https://jsonplaceholder.typicode.com/
Сайт має виглядати так : https://kondrashov.online/images/screens/120.png
На сторінку виводити по 10 коментарів, у низу сторінки зробити поле пагінації (перемикання сторінок) при перемиканні
сторінок показувати нові коментарі.
з коментарів виводити :
"id": 1,
"name"
"email"
"body":
*/

const commentRequest = new XMLHttpRequest();
const url = "https://jsonplaceholder.typicode.com/comments";
commentRequest.open("GET", url);
commentRequest.onreadystatechange = function () {
  if (
    commentRequest.readyState == 4 &&
    commentRequest.status >= 200 &&
    commentRequest.status < 300
  ) {
    const commentData = JSON.parse(commentRequest.responseText);
    let currentPage = 1;
    let rows = 10;

    function displayComment(arrData, rowPerPage, page) {
      const commentsEl = document.querySelector(".comments");
      commentsEl.innerHTML = "";
      page--;

      const start = rowPerPage * page;
      const end = start + rowPerPage;
      const paginatedData = arrData.slice(start, end);

      paginatedData.forEach((el) => {
        const commentEl = document.createElement("div");
        commentEl.classList.add("comment");
        commentEl.innerHTML = `<div><b>id: </b>${el.id}</div><div><b>name: </b>${el.name}</div>
<div><b>email: </b><a href="mailto:${el.email}">${el.email}</a></div><div><b>body: </b>${el.body}</div>`;
        commentsEl.appendChild(commentEl);
      });
    }

    function displayPagination(arrData, rowPerPage) {
      const paginationEl = document.querySelector(".pagination");
      const pagesCount = Math.ceil(arrData.length / rowPerPage);
      const ulEl = document.createElement("ul");
      ulEl.classList.add("pagination__list");

      for (let i = 0; i < pagesCount; i++) {
        const liEl = displayPaginationBtn(i + 1);

        ulEl.appendChild(liEl);
      }
      paginationEl.appendChild(ulEl);
    }

    function displayPaginationBtn(page) {
      const liEl = document.createElement("li");
      liEl.classList.add("pagination__item");
      liEl.innerText = page;

      if (currentPage == page) {
        liEl.classList.add("active");
      }
      liEl.addEventListener("click", () => {
        currentPage = page;
        displayComment(commentData, rows, currentPage);

        let currentItemLi = document.querySelector("li.active");
        currentItemLi.classList.remove("active");

        liEl.classList.add("active");
      });

      return liEl;
    }

    displayComment(commentData, rows, currentPage);
    displayPagination(commentData, rows);
  } else if (commentRequest.readyState == 4) {
    throw new Error("Виникла помилка при завантаженні");
  }
};
commentRequest.send();

// const showComment = (arr) => {
//   const div = document.querySelector(".comment-container");
//   const commentCard = arr
//     .map((el) => {
//       return `
//     <div>id: ${el.id}</div>
//     <div>name: ${el.name}</div>
//     <div>email: <a href="mailto:${el.email}">${el.email}</a></div>
//     <div>body: ${el.body}</div>
//       `;
//     })
//     .join("");
//   div.insertAdjacentHTML("beforeend", commentCard);
// };
