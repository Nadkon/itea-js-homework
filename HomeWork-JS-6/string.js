/*
Реалізуйте клас MyString, який матиме такі методи: метод reverse(),
який параметром приймає рядок, а повертає її в перевернутому вигляді, метод ucFirst(),
який параметром приймає рядок, а повертає цей же рядок, зробивши його першу літеру великою
та метод ucWords, який приймає рядок та робить заголовною першу літеру кожного слова цього рядка.
*/

const MyString = new Object();
MyString.reverse = (str) => {
  let string = str.split("").reverse().join('');
  console.log(string);
};

MyString.ucFirst = (str) =>{
  let newStr = str.charAt(0).toUpperCase().concat(str.slice(1));
  console.log(newStr);
}

MyString.ucWords = (str) => {
  let arr = str.split(' ');
  for (let i = 0; i < arr.length; i++){
    arr[i] = arr[i].charAt(0).toUpperCase() + arr[i].slice(1);
  }
  let newStr = arr.join(' ');
  console.log(newStr);
}
MyString.ucWords('anna likes nadine');
