/*
Створити клас Animal та розширюючі його класи Dog, Cat, Horse.
Клас Animal містить змінні food, location і методи makeNoise, eat, sleep. Метод makeNoise, наприклад, може виводити на консоль "Така тварина спить".
Dog, Cat, Horse перевизначають методи makeNoise, eat.
Додайте змінні до класів Dog, Cat, Horse, що характеризують лише цих тварин.
Створіть клас Ветеринар, у якому визначте метод void treatAnimal(Animal animal). Нехай цей метод роздруковує food і location тварини, що прийшла на прийом.
У методі main створіть масив типу Animal, в який запишіть тварин всіх типів, що є у вас. У циклі надсилайте їх на прийом до ветеринара.
*/

class Animal {
  constructor(food, location) {
    this.food = food;
    this.location = location;
  }
  makeNoise() {
    return `This animal is too noisy!!!`;
  }
  eat() {
    return `This animal eats ${this.food}.`;
  }
  sleep() {
    return `This animal is sleeping.`;
  }
}

class Dog extends Animal {
  constructor(food, location, weight) {
    super(food, location);
    this.weight = weight;
  }
  getWeight() {
    if (this.weight > 10) {
      return `This animal is too fat. We need to correct the diet!`;
    } else if (this.weight > 3) {
      return `Its weight is normal.`;
    } else {
      return `You need to fead the animal better!`;
    }
  }
  dogBark() {
    return `Your dog is barking!`;
  }
  getLocation() {
    return `Your dog is at the ${this.location}.`;
  }

  makeNoise() {
    return this.dogBark() + " " + super.makeNoise();
  }
  eat() {
    return this.getWeight() + " " +  super.eat();
  }
  sleep() {
    return this.getLocation() + " " + super.sleep();
  }
}

const dog = new Dog("Meat", "home", 15);
document.write(`<p style = "color:red">Dog class</p>`)
document.write('<br/>')
document.write(dog.makeNoise());
document.write('<br/>')
document.write(dog.eat());
document.write('<br/>')
document.write(dog.sleep());
document.write('<br/>')
document.write('<hr/>')

class Cat extends Animal{
  constructor(food, location, color) {
    super(food, location);
    this.color = color;
  }
   getColor() {
    if (this.color === "black") {
      return `This animal needs food for black cats.`;
    } else if (this.color === "white") {
      return `This animal needs food for white cats.`;
    } else {
      return `You can fead the cat with the food for black or white cats.`;
    }
  }
  catMew() {
    return `Your cat is mewing loudly!`;
  }
  getLocation() {
    return `Your cat is at the ${this.location}.`;
  }

  makeNoise() {
    return this.catMew() + " " + super.makeNoise();
  }
  eat() {
    return this.getColor() + " " +  super.eat();
  }
  sleep() {
    return this.getLocation() + " " + super.sleep();
  }
}

const cat = new Cat("KitCat", "street", "black");
document.write(`<p style = "color:red">Cat class</p>`)
document.write('<br/>')
document.write(cat.makeNoise());
document.write('<br/>')
document.write(cat.eat());
document.write('<br/>')
document.write(cat.sleep());
document.write('<hr/>')

class Horse extends Animal{
  constructor(food, location, speed) {
    super(food, location);
    this.speed = speed;
  }
     getSpeed() {
      return `Your horse can run ${this.speed} km per hour. You need to fead it better.`;
  }
  hourseNeigh() {
    return `Your horse is neighing loudly!`;
  }
  getLocation() {
    return `Your horse is at the ${this.location}.`;
  }

  makeNoise() {
    return this.hourseNeigh() + " " + super.makeNoise();
  }
  eat() {
    return this.getSpeed() + " " +  super.eat();
  }
  sleep() {
    return this.getLocation() + " " + super.sleep();
  }
}

const horse = new Horse("carrot", "stall", 60);
document.write(`<p style = "color:red">Horse class</p>`)
document.write('<br/>')
document.write(horse.makeNoise());
document.write('<br/>')
document.write(horse.eat());
document.write('<br/>')
document.write(horse.sleep());
document.write('<hr/>')

class Veterinarian {
  constructor(animal) {
    this.animal = animal;
    this.name = this.constructor.name;
  }
       showVeterinarian() {
      document.write(this.constructor.name);
     }
     showAnimal() {
      console.log(this.animal);
    }
      static getAnimals (...animals) {
        let arr = '';
        for (let i in [...animals]) {
          arr += `${animals[i].animal.constructor.name} goess to the doctor; `;
        }
        document.write(`<p style = "color:red">List of animals class</p>`);
        document.write(arr);

    }
  treatAnimal(animal) {
    return `${this.animal.constructor.name} animal eats ${this.animal.food} and is at ${this.animal.location}`;
  }
  }

const animals = [new Dog("Meat", "home", 15), new Cat("KitCat", "street", "black"), new Horse("carrot", "stall", 60)];
const animal1 = new Veterinarian(new Dog("Meat", "home", 15));
const animal2 = new Veterinarian(new Cat("KitCat", "street", "black"));
const animal3 = new Veterinarian(new Horse("carrot", "stall", 60));

document.write(`<p style = "color:red">Veterinarian class</p>`)
document.write('<br/>')
document.write(animal1.treatAnimal());
document.write('<br/>')
document.write(animal2.treatAnimal());
document.write('<br/>')
document.write(animal3.treatAnimal());
document.write('<br/>')
Veterinarian.getAnimals(animal1, animal2, animal3);