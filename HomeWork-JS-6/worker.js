/*
Реалізуйте клас Worker (Працівник), який матиме такі властивості: name (ім'я), surname (прізвище),
rate (ставка за день роботи), days (кількість відпрацьованих днів).
Також клас повинен мати метод getSalary(), який виводитиме зарплату працівника.
Зарплата - це добуток (множення) ставки rate на кількість відпрацьованих днів days.
*/
const worker = {
  name: "Ivan",
  surename: "Ivanov",
  rate: 2000,
  days: 24,
  getSalary: () => {
    console.log(worker.days * worker.rate);
}
}

worker.getSalary()