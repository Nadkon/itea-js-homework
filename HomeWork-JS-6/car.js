/*
Створити клас Car , Engine та Driver.
Клас Driver містить поля - ПІБ, стаж водіння.
Клас Engine містить поля – потужність, виробник.
Клас Car містить поля – марка автомобіля, клас автомобіля, вага, водій типу Driver, мотор типу Engine. Методи start(), stop(), turnRight(), turnLeft(), які виводять на друк: "Поїхали", "Зупиняємося", "Поворот праворуч" або "Поворот ліворуч". А також метод toString(), який виводить повну інформацію про автомобіль, її водія і двигуна.

Створити похідний від Car клас - Lorry (вантажівка), що характеризується також вантажопідйомністю кузова.
Створити похідний від Car клас - SportCar, який також характеризується граничною швидкістю.
*/


// Driver
class Person {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }
}

class Driver extends Person {
  constructor(name, age, experience) {
    super(name, age);
        this.experience = experience;
  }
   getDriverInfo = function() {
     return `The driver's name is ${this.name}. He is ${this.age} years old. He has ${this.experience} years of experiences`;
    }
}


// Engine
class Engine {
  constructor(power, manufacture) {
    this.power = power;
    this.manufacture = manufacture;
  }
  getEngineInfo = function () {
    return`The power of this car is ${this.power}. The manufacture is ${this.manufacture}`;
  }
}

// Car
class Car {
  constructor(engine, driver, autoModel, autoClass, autoWeight) {
    this.autoModel = autoModel;
    this.autoClass = autoClass;
    this.autoWeight = autoWeight;
    this.driver = driver;
    this.engine = engine;
  }
  toString() {
    return `${this.driver.getDriverInfo()}. ${this.engine.getEngineInfo()}. The model's name is ${this.autoModel}. The class of this car is ${this.autoClass}, the weight is ${this.autoWeight}`;
  }
}

Car.prototype.start = function () {
  return "Поїхали";
}
Car.prototype.stop = function () {
  return "Зупиняємося";
}
Car.prototype.turnRight = function () {
  return "Поворот праворуч";
}

Car.prototype.turnLeft = function () {
  return "Поворот ліворуч";
}

// Lorry
class Lorry extends Car {
  constructor(engine, driver, autoModel, autoClass, autoWeight, carrying) {
    super(›, driver, autoModel, autoClass, autoWeight);
    this.carrying = carrying;
  }
   getCarrying() {
      return `The carrying is ${this.carrying}`;
    }
  toString() {
   return super.toString() + ". " + this.getCarrying();
  }
}

// sportcar
class SportCar extends Car{
  constructor(engine, driver, autoModel, autoClass, autoWeight, speed) {
    super(engine, driver, autoModel, autoClass, autoWeight);
    this.speed = speed;
  }
     getSpeed() {
      return `The speed is ${this.speed}`;
    }
  toString() {
   return super.toString() + ". " + this.getSpeed();
  }
}
const sportCar = new SportCar (new Engine(200, "Manufacture"), new Driver('Alex', 60, 20), "model", "A", 250, "SPEED");
document.write(sportCar.toString());
document.write(`<br/>`);
const lorry = new Lorry (new Engine(200, "Manufacture"), new Driver('Alex', 60, 20), "model", "A", 250, "carrying");
document.write(lorry.toString());

