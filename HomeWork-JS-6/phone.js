/*
Створіть клас Phone, який містить змінні number, model і weight.
Створіть три екземпляри цього класу.
Виведіть на консоль значення їх змінних.
Додати в клас Phone методи: receiveCall, має один параметр - ім'я. Виводить на консоль повідомлення "Телефонує {name}". Метод getNumber повертає номер телефону. Викликати ці методи кожного з об'єктів.
*/
class Phone {
  constructor(number, model, weigh) {
    this.number = number;
    this.model = model;
    this.weigh = weigh;
  }
}
Phone.prototype.phone = function (name) {
  document.write(`${name} is calling`)
  document.write(`<br />`);
};
Phone.prototype.getNumber = function () {
  document.write(`The phone numbr is ${this.number}`);
};

const objec1 = new Phone(38099999900, "Samsung", "150g");
const objec2 = new Phone(38067000000, "iPhone", "120g");
const objec3 = new Phone(38063002398, "XiaoMe", "130g");

console.log(objec1);
console.log(objec2);
console.log(objec3);

let callerName1 = prompt("enter the name");
objec1.phone(callerName1);
objec1.getNumber();
document.write(`<br />`);

let callerName2 = prompt("enter the name");
objec2.phone(callerName2);
objec2.getNumber();
document.write(`<br />`);

let callerName3 = prompt("enter the name");
objec3.phone(callerName3);
objec3.getNumber();


// const Phone = new Object();
// Phone.number = 38099999999;
// Phone.model = 'Samsung';
// Phone.weight = '150g';

