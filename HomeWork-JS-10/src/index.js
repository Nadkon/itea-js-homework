/*
* У папці calculator дана верстка макета калькулятора.
Потрібно зробити цей калькулятор робочим.
* При натисканні на клавіші з цифрами - набір введених цифр має бути показаний на табло калькулятора.
* При натисканні на знаки операторів (`*`, `/`, `+`, `-`) на табло нічого не відбувається - програма чекає введення другого числа для виконання операції.
* Якщо користувач ввів одне число, вибрав оператор і ввів друге число, то при натисканні як кнопки `=`, так і будь-якого з операторів, в табло повинен з'явитися результат виконання попереднього виразу.
* При натисканні клавіш `M+` або `M-` у лівій частині табло необхідно показати маленьку букву `m` - це означає, що в пам'яті зберігається число. Натискання на MRC покаже число з пам'яті на екрані. Повторне натискання `MRC` має очищати пам'ять.
*/

/*
1. Зєжнати верстку з джс
Потрібно зробити цей калькулятор робочим.
2. Знайти всі кнопки та спрбувати їх вивести у консоль
3. Записти перші числа в память коду
4. вивести числа на екран
5. додати знаки ар. операцій
6. Знайти другі числа
7. Вивести другі числа
8. Вивести результат операції
*/

const calculate = {
  operand1: "",
  sign: "",
  operand2: "",
  rez: "",
  mem: "",
};

// https://regexr.com/

// memory button
let memorySign = document.querySelector(".memory-sign");

document.querySelector(".keys").addEventListener("click", (e) => {
  // digits entry
  if (validate(/^[\d\.]$/, e.target.value)) {
    // operand 1 entry
    if (calculate.operand2 === "" && calculate.sign === "") {
      calculate.operand1 += e.target.value;
      if (e.target.value.includes(".")) {
        document.querySelector(".point").disabled = true;
      }
      show(calculate.operand1);
    } else {
      // operand 2 entry
      calculate.operand2 += e.target.value;
      if (e.target.value.includes(".")) {
        document.querySelector(".point").disabled = true;
      }
      show(calculate.operand2);
      document.querySelector(".orange").disabled = false;
    }
  }
  // sing symbol entry
  else if (validate(/^[+-/*]$/, e.target.value)) {
    calculate.sign = e.target.value;
    document.querySelector(".point").disabled = false;
  } else if (validate(/^=$/, e.target.value)) {
    switch (calculate.sign) {
      case "+":
        calculate.rez = +calculate.operand1 + +calculate.operand2;
        break;
      case "-":
        calculate.rez = +calculate.operand1 - +calculate.operand2;
        break;
      case "*":
        calculate.rez = +calculate.operand1 * +calculate.operand2;
        break;
      case "/":
        if (calculate.operand2 === "0") {
          calculate.rez = "MISTAKE";
          calculate.operand1 = "";
          calculate.operand2 = "";
          calculate.sign = "";
          return;
        }
        calculate.rez = +calculate.operand1 / +calculate.operand2;
        break;
    }
    show(calculate.rez);
    document.querySelector(".orange").disabled = true;
    calculate.operand1 = "";
    calculate.operand2 = "";
    calculate.sign = "";
    document.querySelector(".point").disabled = false;
  }
  // remove all entries except memory
  else if (validate(/\C/, e.target.value)) {
    calculate.operand1 = "";
    calculate.operand2 = "";
    calculate.sign = "";
    show("");
  } else if (validate(/\m\+/, e.target.value)) {
    calculate.mem = document.querySelector(".display input").value;
    document.querySelector(".display input").value = "";
    memorySign.style.display = "inline";
    if (calculate.operand2 != "") {
      calculate.operand2 = "";
    } else {
      calculate.operand1 = "";
    }
  } else if (validate(/\mrc/, e.target.value)) {
    show(calculate.mem);
    if (calculate.operand1 == "") {
      calculate.operand1 = calculate.mem;
    } else {
      calculate.operand2 = calculate.mem;
      document.querySelector(".orange").disabled = false;
    }
  } else if (validate(/\m\-/, e.target.value)) {
    calculate.mem = "";
    document.querySelector(".display input").value = "";
    memorySign.style.display = "none";
  }
});

function show(v) {
  const d = document.querySelector(".display input");

  d.value = v;
}

const validate = (r, v) => r.test(v);
