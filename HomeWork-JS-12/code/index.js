import {
  pizzaSelectSize,
  pizzaSelectTopping,
  show,
  validate,
  bannerEscape,
  deleteTopping,
  deleteSauce,
} from "./functions.js";
import { pizzaUser } from "./data-pizza.js";

// form validation
document.querySelectorAll(".grid input").forEach((input) => {
  if (input.type === "text" || input.type === "tel" || input.type === "email") {
    input.addEventListener("change", () => {
      if (input.type === "text" && validate(/^[А-я-іїґє]{2,}$/i, input.value)) {
        selectInput(input, pizzaUser);
      } else if (
        input.type === "tel" &&
        validate(/^\+380\d{9}$/, input.value)
      ) {
        selectInput(input, pizzaUser);
      } else if (
        input.type === "email" &&
        validate(/^[a-z0-9_.]{3,}@[a-z0-9._]{2,}\.[a-z.]{2,9}$/i, input.value)
      ) {
        selectInput(input, pizzaUser);
      } else {
        input.classList.add("error");
      }
    });
  } else if (input.type === "reset") {
    input.addEventListener("click", () => {});
  } else if (input.type === "button") {
    input.addEventListener("click", () => {
      localStorage.userInfo = JSON.stringify(pizzaUser);
    });
  }
});

// class change for form validation
function selectInput(input, data) {
  input.className = "";
  input.classList.add("success");
  data.userName = input.value;
}

document.querySelector("#pizza").addEventListener("click", pizzaSelectSize);

// Drag&Drop
document.querySelector(".ingridients").addEventListener(
  "dragstart",
  function (e) {
    e.target.style.backgroundColor = "grey";
    e.dataTransfer.effectAllowed = "move";
    e.dataTransfer.setData("Text", e.target.id);
  },
  false
);

document.querySelector(".ingridients").addEventListener(
  "dragend",
  function (e) {
    e.target.style.backgroundColor = "";
  },
  false
);

document.querySelector(".table").addEventListener(
  "dragenter",
  function (e) {
    this.style.border = "3px solid grey";
  },
  false
);

document.querySelector(".table").addEventListener(
  "dragleave",
  function (e) {
    this.style.border = "";
  },
  false
);

document.querySelector(".table").addEventListener(
  "dragover",
  function (e) {
    if (e.preventDefault) e.preventDefault();
    return false;
  },
  false
);

document.querySelector(".table").addEventListener(
  "drop",
  function (e) {
    if (e.preventDefault) e.preventDefault();
    if (e.stopPropagation) e.stopPropagation();

    this.style.border = "";
    var id = e.dataTransfer.getData("Text");
    var elem = document.getElementById(id);

    pizzaSelectTopping(elem);

    return false;
  },
  false
);

banner.addEventListener("mouseover", bannerEscape);

show(pizzaUser);
document.body.addEventListener("click", deleteTopping);
document.body.addEventListener("click", deleteSauce);
