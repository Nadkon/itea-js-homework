export const pizzaBD = {
  size: [
    { name: "small", price: 40 },
    { name: "mid", price: 55 },
    { name: "big", price: 70 },
  ],
  topping: [
    { name: "moc1", price: 40, productName: "Сир звичайний", count: 0 },
    { name: "moc2", price: 40, productName: "Сир фета", count: 0 },
    { name: "moc3", price: 40, productName: "Моцарелла", count: 0 },
    { name: "telya", price: 65, productName: "Телятина", count: 0 },
    { name: "vetch1", price: 35, productName: "Помiдори", count: 0 },
    { name: "vetch2", price: 37, productName: "Гриби", count: 0 },
  ],
  sauce: [
    { name: "sauceClassic", price: 30, productName: "Кетчуп" },
    { name: "sauceBBQ", price: 30, productName: "BBQ" },
    { name: "sauceRikotta", price: 30, productName: "Рiкотта" },
  ],
};

export const pizzaUser = {
  size: { name: "big", price: 70 },
  sauce: "",
  topping: [],
  price: "",
  userPhone: "",
  userEmail: "",
  userName: "",
  data: "",
};
