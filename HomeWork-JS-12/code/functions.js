import { pizzaUser, pizzaBD } from "./data-pizza.js";

const table = document.querySelector(".table");

const pizzaSelectSize = (e) => {
  if (e.target.tagName === "INPUT" && e.target.checked) {
    pizzaUser.size = pizzaBD.size.find((el) => el.name === e.target.id);
  }
  show(pizzaUser);
};

const toppingUpdate = (name) => {
  if (pizzaUser.topping.length) {
    if (
      pizzaUser.topping.find(
        (el) => el === pizzaBD.topping.find((el) => el.name === name)
      )
    ) {
      pizzaUser.topping.map((el) => {
        if (el.name === name) el.count = (el.count || 0) + 1;
      });
    } else {
      let el = pizzaBD.topping.find((el) => el.name === name);
      el.count = 1;
      pizzaUser.topping.push(el);
    }
  } else {
    pizzaUser.topping.push(pizzaBD.topping.find((el) => el.name === name));
    pizzaUser.topping[0].count = 1;
  }
};

const pizzaSelectTopping = (e) => {
  // switch (e.target.id)
  // in find ---> el => el.name === e.target.id

  switch (e.id) {
    case "sauceClassic":
      pizzaUser.sauce = pizzaBD.sauce.find((el) => el.name === e.id);
      break;
    case "sauceBBQ":
      pizzaUser.sauce = pizzaBD.sauce.find((el) => el.name === e.id);
      break;
    case "sauceRikotta":
      pizzaUser.sauce = pizzaBD.sauce.find((el) => el.name === e.id);
      break;

    case "moc1":
      toppingUpdate(e.id);
      break;
    case "moc2":
      toppingUpdate(e.id);
      break;
    case "moc3":
      toppingUpdate(e.id);
      break;
    case "telya":
      toppingUpdate(e.id);
      break;
    case "vetch1":
      toppingUpdate(e.id);
      break;
    case "vetch2":
      toppingUpdate(e.id);
      break;
  }

  show(pizzaUser);

  if (e.tagName === "IMG") {
    if (
      (e.id === "sauceClassic" ||
        e.id === "sauceBBQ" ||
        e.id === "sauceRikotta") &&
      table.getElementsByTagName("img")[1]
    ) {
      table.lastElementChild.remove();
    }
    table.insertAdjacentHTML(
      "beforeend",
      `<img src="${e.src}" data-name='${e.id}'>`
    );
  }
};

function show(pizza) {
  const price = document.getElementById("price");
  const sauce = document.getElementById("sauce");
  const topping = document.getElementById("topping");

  let totalPrice = 0;
  if (pizza.size !== "") {
    totalPrice += parseFloat(pizza.size.price);
  }
  if (pizza.sauce !== "") {
    totalPrice += parseFloat(pizza.sauce.price);
  }
  if (pizza.topping.length) {
    totalPrice += pizza.topping.reduce((a, b) => a + b.price * b.count, 0);
  }
  price.innerText = totalPrice;

  if (pizza.sauce !== "") {
    sauce.innerHTML = `<span class="topping">${pizza.sauce.productName} <button type='button' class='delete-sauce'></button></span>`;
  }

  if (Array.isArray(pizza.topping)) {
    topping.innerHTML = pizza.topping
      .map((el) => {
        if (el.count > 1) {
          return `<span class="topping" data-count=${el.count}>${el.productName}<button type="button" class="delete-topping"></button></span>`;
        } else {
          return `<span class="topping">${el.productName}<button type="button" class="delete-topping"></button></span>`;
        }
      })
      .join("");
  }

  pizzaUser.price = totalPrice;
  pizzaUser.data = new Date();
}

const validate = (pattern, value) => pattern.test(value);

const bannerEscape = (e) => {
  const banner = document.querySelector("#banner");
  banner.style.bottom = Math.abs(Math.floor(Math.random() * 100 - 15)) + "%";
  banner.style.right = Math.abs(Math.floor(Math.random() * 100 - 15)) + "%";
};

// Toping deletion
const deleteTopping = (e) => {
  if (e.target.closest(".delete-topping")) {
    let btn = e.target.closest(".delete-topping");
    let selectedTopping = btn.parentElement;
    let type = pizzaBD.topping.find(
      (el) => el.productName === selectedTopping.textContent
    );

    if (selectedTopping.dataset.count) {
      selectedTopping.dataset.count -= 1;
    } else {
      selectedTopping.dataset.count = 0;
    }
    pizzaUser.topping.map((el) => {
      if (el.productName === selectedTopping.textContent) {
        let currentImg = document.querySelectorAll(`[data-name='${el.name}']`);

        currentImg[currentImg.length - 1].remove();
        el.count -= 1;
      }
    });
    if (selectedTopping.dataset.count == 0) {
      pizzaUser.topping.splice(pizzaUser.topping.indexOf(type), 1);
      selectedTopping.remove();
    }
    pizzaUser.price -= type.price;
    price.innerText = pizzaUser.price;
  }
};

const deleteSauce = (e) => {
  if (e.target.closest(".delete-sauce")) {
    let btn = e.target.closest(".delete-sauce");
    let selectedTopping = btn.parentElement;
    pizzaUser.topping.map((el) => {
      if (el.productName === selectedTopping.textContent) {
        el.count -= 1;
      }
    });

    selectedTopping.remove();
    pizzaUser.price -= pizzaUser.sauce.price;
    price.innerText = pizzaUser.price;
    document.querySelector("[data-name*=sauce]").remove();
    pizzaUser.sauce = "";
    console.log(pizzaUser);
  }
};

export {
  pizzaSelectSize,
  pizzaSelectTopping,
  show,
  validate,
  bannerEscape,
  deleteTopping,
  deleteSauce,
};
